class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :body
      t.boolean :flag
      t.references :profile, index: true
      t.references :post, index: true

      t.timestamps null: false
    end
    add_foreign_key :comments, :profiles
    add_foreign_key :comments, :posts
  end
end
