class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.references :profile, index: true
      t.references :post, index: true

      t.timestamps null: false
    end
    add_foreign_key :likes, :profiles
    add_foreign_key :likes, :posts
  end
end
