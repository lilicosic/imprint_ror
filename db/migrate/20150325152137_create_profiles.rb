class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.references :user, index: true
      t.boolean :flag, default: false
      t.string :location
      t.string :avatar_url

      t.timestamps null: false
    end
    add_foreign_key :profiles, :users
  end
end
