class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :content
      t.references :user, index: true
      t.text :images
      t.boolean :flag, default: false
      t.boolean :private, default: false
      t.integer :likes, default: 0

      t.timestamps null: false
    end
    add_foreign_key :posts, :users
  end
end
