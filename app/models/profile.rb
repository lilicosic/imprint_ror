class Profile < ActiveRecord::Base
  require "open-uri"
  has_many :comments
  belongs_to :user
  
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "40x40>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates :name, presence: true, length: {in: 1..20 }
  validates :user_id, presence: true
  
  #friends
  has_many :active_friends, class_name: "Friend", foreign_key: "follower_id", dependent: :destroy
  has_many :following, through: :active_friends, source: :followed
  has_many :passive_friends, class_name: "Friend", foreign_key: "followed_id", dependent: :destroy
  has_many :followers, through: :passive_friends
  
  def following?(other_user)
     following.include?(other_user)
   end

   def follow(other_user)
     active_friends.create(followed_id: other_user.id)
   end

   def unfollow(other_user)
     active_friends.find_by(followed_id: other_user.id).destroy
   end
  
   #avatar
  def picture_from_url(url)
    self.picture = URI.parse(url)
  end
  
end
