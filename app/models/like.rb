class Like < ActiveRecord::Base
  belongs_to :profile
  belongs_to :post
  
  validates :profile_id, presence: true
  validates :post_id, presence: true
  
end
