class Comment < ActiveRecord::Base
  belongs_to :profile
  belongs_to :post
  
  validates :body, presence: true, length: {in: 1..140}
  validates :profile_id, presence: true
  validates :post_id, presence: true
end
