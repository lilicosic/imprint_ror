class Post < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  
  validates :content, presence: true, length: { maximum: 300 }
  validates :user_id, presence: true
  
  has_attached_file :photo, :styles => { :medium => "500x500>", :small => "200x200>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/
  
end
