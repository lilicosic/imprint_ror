class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :posts
  has_one :profile
  
  #devise                                
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  accepts_nested_attributes_for :profile
           
end
