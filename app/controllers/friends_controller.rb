class FriendsController < ApplicationController
  
    def create
      user = Profile.find(params[:followed_id])
      current_user.profile.follow(user)
      redirect_to user
    end

    def destroy
      user = Friend.find(params[:id]).followed
      current_user.profile.unfollow(user)
      redirect_to user
    end
  
end
