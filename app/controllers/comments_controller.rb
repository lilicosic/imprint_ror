class CommentsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create]
  
  def show
    
  end
  
  def new
  end
  
  def create
    @profile = Profile.where(:id => current_user.id).first
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(comment_params)
    puts @profile
    @comment.profile = @profile
    if @comment.save
      redirect_to post_path(@post)
    end
    
  end
  
  private
  def comment_params
    params.require(:comment).permit(:body)
  end
end
