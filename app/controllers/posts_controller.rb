class PostsController < ApplicationController
  
  def index
    if user_signed_in?
      @posts = Post.where(:user_id => current_user.id).paginate(:page => params[:page],:per_page => 5).order("created_at DESC")
          
    end
  end
  
  def show
    @post = Post.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @post }
     end
    
  end
  
  def new
    @post = Post.new
  end
  
  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to @post
    else
      render 'new'
    end
  end
  
  private
  def post_params
    params.require(:post).permit(:content, :photo)
  end
  
end
