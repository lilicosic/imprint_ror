class ProfilesController < ApplicationController
  
  before_action :authenticate_user!, only: [:new, :create, :update]
  
  def show
    @profile = Profile.where(:user_id => params[:id]).first
    @posts = Post.where(:user_id => params[:id]).paginate(:page => params[:page],:per_page => 5).order("created_at DESC")

  end
  
  def new
    @profile = Profile.new
  end
  
  def create
    @profile = Profile.new(profile_params)
    @profile.user_id = current_user.id
    if @profile.save
      redirect_to profile_path(@profile.user_id)
    end

  end
  
  def edit
    @profile = Profile.where(:user_id => params[:id]).first
  end
  
  def update    
    @profile = Profile.where(:user_id => params[:id]).first
    
    if @profile.update_attributes(profile_params)
      redirect_to profile_path(@profile.user_id)
    else
      render 'edit'
    end
    
  end
  
  def following
      @title = "Following"
      @user  = Profile.find(params[:id])
      @users = @user.following.paginate(page: params[:page],:per_page => 5)
      render 'show_follow'
    end

    def followers
      @title = "Followers"
      @user  = Profile.find(params[:id])
      @users = @user.followers.paginate(page: params[:page],:per_page => 5)
      render 'show_follow'
    end

  private
  def profile_params
    params.require(:profile).permit(:name, :location, :avatar)
  end
  
end
