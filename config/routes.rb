Rails.application.routes.draw do
  
  resources :profiles
  resources :posts
  resources :friends, only: [:create, :destroy]
  
  resources :profiles do
    member do
      get :following, :followers
    end
  end
  
  
  resources :posts do
    resources :comments
  end
  
  resources :relationships, only: [:create, :destroy]
  devise_for :users, :controllers => { :registrations => "registrations" }
  
  
  root 'static_pages#front'
  get 'users/new'  
  get 'static_pages/front'
  get 'static_pages/about'
  
end
